const app = getApp()
// pages/bookinfo/bookinfo.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    book:{},
    start: ['unstart', 'unstart', 'unstart', 'unstart','unstart'],
    source:"",
    lastChapterName:"",
    clickvolume:0,
    totalChapter:0,
    totalCollection:0,
    comment_num:0,
    comment:"",
    userName:"",
    isFold: true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    var data = JSON.parse(options.data);
    var time = app.util.timestampToTime(data.novel.author.startPenDate)
    data.novel.author.startPenDate = time


    this.setData({
      book: data
    })
 
    const title = data.novel.name
    wx.setNavigationBarTitle({
      title: title
    })



   var  starts = this.data.start
    //获取小说有评分几个星
    for(var i= 0 ; i< 5;i++){
      if(i>=5){
        break
      }
      starts[i]='start'
    }

    this.setData({
      'start': starts,
    })




    var that = this


    //获取小说信息
    app.sendRequest({
      url: 'novel/info',
      method: 'get',
      data: { 'nid': that.data.book.novel.novelId},
      succfn: function (data) {
        if(data.code==1000){
            that.setData({
              "comment_num": data.datas.comment,
              "lastChapterName": data.datas.lastChapterName,
              "source": data.datas.source,
              "totalChapter": data.datas.totalChapter,
              "totalCollection": data.datas.totalCollection

            })
    
        }
      },
      failfn: function (data) {
        console.log(data)
      }

    })

    //获取评论
    app.sendRequest({
      url: 'comments/novel/',
      method: 'post',
      data: {
        "pageSize": 1,
        "pageNum": 1,
        "novelId": that.data.book.novel.novelId
      },
      header:{
        'content-type': 'application/json;'
      },
      succfn: function (data) {
        if (data.code == 1000 && data.datas.comments.length==1) {

          that.setData({
            "comment": data.datas.comments[0].commentsContent,
            "userName": data.datas.comments[0].user.account
          })
        }
      },
      failfn: function (data) {
        console.log(data)
      }

    })





  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },

  zh:function(){
    this.setData({
      unfold:''
    })
  },

  sf:function(){
    this.setData({
      unfold: 'content'
    })
  },
  showAll:function(){

    this.setData({
      isFold: !this.data.isFold,
    })
  },
  read:function(){
    var that=this

    wx.navigateTo({
      url: '../read/read?novel=' + JSON.stringify(that.data.book.novel) 
    })
   

 
  }

})