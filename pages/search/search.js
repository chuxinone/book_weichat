const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    placeholder:'请输入关键词',
    value:[],
    center:true,
    show:false,
    result:[],
    historyData:[]
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.history()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
   _handleZanFieldChange: function (e) {
     var that=this
     that.setData({
       'value': e.detail.value
     })
     console.log(e.detail.value)
     app.sendRequest({
       url: 'novel/association',
       method: 'get',
       data: { "name": e.detail.value},
       succfn: function (data) {
          //显示搜索结果
         that.setData({
           'result': data.datas
          })
       },
       failfn: function (data) {
         console.log(data)
       }
     })



   

    // 控制关闭按钮的显示逻辑
    if (!this.data.show) {
      this.setData({
        'show': true,
        'center':false
      })
    }

    if (e.detail.cursor == 0) {
      this.setData({
        'show': false,
        'center': true
      })
    }


  },
  search: function () {
    this.setData({
      'show': false,
      'center': true
    })

    this.addHistory(this.data.value)
    wx.navigateTo({
      url: '../resultlist/resultlist?titile=' + this.data.value
    })
  },

  clear: function () {
    this.setData({
      'value': '',
      'show': false,
      'center':true
    })
  },
  //跳转逻辑
  resultlist:function(e){
    this.addHistory(e.currentTarget.dataset.bookname)
    wx.navigateTo({
      url: '../resultlist/resultlist?titile=' + e.currentTarget.dataset.bookname
    })
  },
  //搜索历史
  history:function(){
    var value = wx.getStorageSync('history')
    if(value==""){
      wx.setStorageSync('history', [])
    }else{
       this.setData({
         'historyData':value
       })
    }

    var dhistoryDataata = this.data.historyData

    console.log(dhistoryDataata)
  },
  //添加历史
  addHistory:function(name){
    
    var historyData = this.data.historyData
  

    for(var i = 0 ; i < historyData.length;i++){
        if(historyData[i]==name){
          return
        }
    }

    historyData.push(name)
    if(historyData.length>5){
      historyData.shift()
    }

    this.setData({
      "historyData": historyData
    })
    wx.setStorageSync('history', this.data.historyData)
  },
  /**
   * 清空历史
   */
  removeSHistory:function(){
    this.setData({
      "historyData": []
    })
    wx.removeStorageSync('history')
  }
})