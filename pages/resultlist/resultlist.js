// pages/resultlist/resultlist.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    bookName:'',
    localBook:[],
    onlineBook:[],
    supports:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const title = '跟“' + options.titile+'”有关的书籍'
    wx.setNavigationBarTitle({
      title: title
    })

    this.setData({
      "bookName": options.titile
    })

    this.search(options.titile)
  },

  /**
   * 搜索
   */
  search:function(name){
    console.log(name)
    var that=this
    app.sendRequest({
      url: 'novel/search',
      method: 'get',
      data: { "name": name },
      succfn: function (data) {
        //显示搜索结果
        if(data.code!=-1){


          var localBook = data.datas.local;

          var onlineBook =data.datas.online

          if(localBook==null || onlineBook==null){
            return
          }

          //处理时间
          for(var i=0; i < localBook.length;i++){
            localBook[i].updateTime = app.util.timestampToTime(localBook[i].updateTime) 
          } 

          var urls=[]
          for (var i = 0; i < onlineBook.length; i++) {
            urls.push(onlineBook[i].putoUrl)
          } 

          that.isSupport(urls)
 
          that.setData({
            'localBook': localBook,
            'onlineBook': onlineBook
          })

        }
   
      },
      failfn: function (data) {
        console.log(data)
      }
    })
  },

  /**
   * 查看是否支持解析
   */
  isSupport:function(datas){
    var that=this
    app.sendRequest({
      url: 'novel/support',
      method: 'post',
      data: datas,
      header: { "Content-Type":"application/json"},
      succfn: function (data) {
          if(data.code!=-1){
            //创建2个变量分别为：支持的、不支持的
            var onlineBook = that.data.onlineBook
            var support_Books=[]
            var notSupport_Books=[]

            //遍历并分别传值给2个变量
            for (var i = 0; i < onlineBook.length; i++) {
              var book = onlineBook[i]
              book.support = data.datas[i]
              if (data.datas[i]){
                support_Books.push(book )
              }else{
                notSupport_Books.push(book)
              }
         
            }

            //合并
            support_Books=support_Books.concat(notSupport_Books)

            //设置
            that.setData({
              onlineBook: support_Books
            })

          }
      },
      failfn: function (data) {
        console.log(data)
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },

  //本地图书跳转
  localInfo: function (ops){


  },
  /**
   * 在线图书处理
   */
  onlineInfo:function(ops){
    var index = ops.currentTarget.dataset.index
    var that = this
    wx.setClipboardData({
      data: that.data.onlineBook[index].putoUrl,
      success(res) {
        wx.getClipboardData({
          success(res) {
            console.log(res.data) // data
          }
        })
      }
    })

  },
  /**
   * 添加去解析
   */
  addbook: function (ops){
    var url = ops.currentTarget.dataset.url


    var that = this
    app.sendRequest({
      url: 'novel/add',
      method: 'get',
      data: { 'url': url},
      succfn: function (data) {
        if(data.code==1000){
          setTimeout(app.util.showSuccess, 200, "请求成功")
        }
       
      }
    })
  },
  bookinfo:function(ops){
    var data=ops.currentTarget.dataset
    var that = this
    app.sendRequest({
      url: 'novel/' + that.data.localBook[data.index].novelId,
      method: 'get',
      succfn: function (data) {
        var novel = {
          "novel": data.datas
        }
        wx.navigateTo({
          url: '../bookinfo/bookinfo?data=' + JSON.stringify(novel)
        })
      },
      failfn: function (data) {
        console.log(data)
      }
    })

  
  }



})