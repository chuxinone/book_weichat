//index.js
//获取应用实例
const app = getApp()

Page({
  data: {
    swiper:[],
    userInfo: {},
    ranking:[],
    hasUserInfo: false,
    newBookName:'天中国器材',
    loding:false,
    canIUse: wx.canIUse('button.open-type.getUserInfo')
  },

  initSwiper:function(){
    var that = this
    //初始化轮播数据
    app.sendRequest({
      url: 'carousel/now',
      method: 'get',
      succfn: function (data) {
        that.setData({
          swiper: data.datas
        })

        console.log(data.datas)
      },
      failfn: function (data) {
        console.log(data)
      }

    })

  },
  //获取排行榜
  initRankingName:function(){
    var that = this
    app.sendRequest({
      url: 'ranking/activty/name',
      method: 'get',
      succfn: function (data) {
        for (var j = 0; j < data.datas.length; j++){
          // console.log(data.datas[j].name)
          that.getRankingNovel(data.datas[j].name)
        }

        // that.getRankingNovel(data.datas[0].name)

    
      },
      failfn: function (data) {
        console.log(data)
      }
    })

  },
  //通过排行榜获取一组小说
  getRankingNovel:function(name){
    var that = this
    app.sendRequest({
      url: 'ranking/activty/get',
      data:{"name":name},
      method: 'get',
      succfn: function (data) {
        if (data.datas.length==0){
          return
        }
      
       var r=that.data.ranking
        var arr={
          "name":name,
          "data":data.datas
        }
    
        r.push(arr)

        that.setData({
          ranking:r
        })
      },
      failfn: function (data) {
        console.log(data)
      }
    })


  },
  onLoad: function () {
    this.initSwiper()
    this.initRankingName()

  },
  //隐藏
  onHide:function(){
    this.setData({
      "loding":true
    })
  },
  //显示加载
  onShow:function (){
    if(this.data.loding){
      console.log("加载一次")
      this.initSwiper()
    }
  },
  getUserInfo: function(e) {
    console.log(e)
    app.globalData.userInfo = e.detail.userInfo
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  },
  bookinfoSwiper: function (ops) {

    var num=ops.currentTarget.dataset.num
    wx.navigateTo({
      url: '../bookinfo/bookinfo?data=' + JSON.stringify(this.data.swiper[num])
    })
  },
  bookinfo:function(ops){
    var name = ops.currentTarget.dataset.name
    var index = ops.currentTarget.dataset.index
    var ranking = this.data.ranking[name].data[index]
    app.sendRequest({
      url: 'novel/' + ranking.novelId,
      method: 'get',
      succfn: function (data) {
        var novel = {
          "novel": data.datas
        }
        wx.navigateTo({
          url: '../bookinfo/bookinfo?data=' + JSON.stringify(novel)
        })
      },
      failfn: function (data) {
        console.log(data)
      }
    })



  },
  search:function(){
    wx.navigateTo({
      url: '../search/search'
    })
  }
})
