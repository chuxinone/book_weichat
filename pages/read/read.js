// pages/read/read.js
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    readerCss: {
      titleSize: 20,    //标题大小
      contentSize: 16,  //内容大小
      titleColor:'',    //标题颜色
      color: '', //内容颜色
      lineHeight: 60,
      backgroundColor: '' //背景色  #C7EDCC 护眼色  #080C10 黑夜
    },
    //主题
    theme:'',
    spacing:'0',
    chapter:null,
    context:'',
    chapterNo:0,
    book:{},
    titile:'章节标题',
    table:false,
    menu:false,
    fontSize:0,
    comTable:null,
    nextClick:'',
    preClick: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    //接受参数
    var novel = JSON.parse(options.novel);

    //加载主题
    var theme=wx.getStorageSync("readTheme")
    // console.log(theme)
    if(theme==null || theme==""){
      theme = "default"
      wx.setStorageSync("readTheme", theme)
    }
    
    //获取字体大小
    var fontSize = wx.getStorageSync("font-size")
    if (fontSize == null || fontSize == "") {
      fontSize = 20
    }


    this.setData({
      theme: theme,
      fontSize: fontSize,
      book:novel
    })

    //加载章节
    this.loadChapter()

    this.addLength()

    //设置标题
    wx.setNavigationBarTitle({
      title: this.data.book.name
    })


  },


  /**
   * 默认的看书模式
   */
  defaultRead:function(){

    //设置背景
    this.data.readerCss.backgroundColor ='#f2efeb'
    //设置内容文本
    this.data.readerCss.color = '#5c5d58'
    //设置章节标题
    this.data.readerCss.titleColor='#000000'
    //设置标题样式
    wx.setNavigationBarColor({
      frontColor: this.data.readerCss.titleColor,
      backgroundColor: this.data.readerCss.backgroundColor,
      animation: {
        duration: 100,
        timingFunc: 'easeIn'
      }
    })

    
    this.setData({
      readerCss: this.data.readerCss
    })


    wx.setStorageSync("readTheme", "default")

  },

  /**
   * 夜间模式
   */
  atnight: function () {

    //设置背景
    this.data.readerCss.backgroundColor = '#1d1d1d'
    //设置内容文本
    this.data.readerCss.color = '#666666'
    //设置章节标题
    this.data.readerCss.titleColor = '#666666'
    //设置标题样式
    wx.setNavigationBarColor({
      frontColor: '#ffffff',
      backgroundColor: this.data.readerCss.backgroundColor,
      animation: {
        duration: 100,
        timingFunc: 'easeIn'
      }
    })


    this.setData({
      readerCss: this.data.readerCss
    })
    wx.setStorageSync("readTheme", "atnight")

  },
  /**
   * 护眼模式
   */
  green:function(){
    //设置背景
    this.data.readerCss.backgroundColor = '#c7edcc'
    //设置内容文本
    this.data.readerCss.color = '#233323'
    //设置章节标题
    this.data.readerCss.titleColor = '#000000'
    //设置标题样式
    wx.setNavigationBarColor({
      frontColor: this.data.readerCss.titleColor,
      backgroundColor: this.data.readerCss.backgroundColor,
      animation: {
        duration: 100,
        timingFunc: 'easeIn'
      }
    })


    this.setData({
      readerCss: this.data.readerCss
    })
    wx.setStorageSync("readTheme", "green")

  },
  //判断是否需要增长
  addLength:function(){
    var that=this;
    var context=this.data.context
    if (context.length<=1000){
      wx.getSystemInfo({
        success(res) {
          that.setData({
            spacing: res.windowHeight-200
          })
        }
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.switchTheme(this.data.theme)

  },
  /**
   * 获取上次的章节
   */
  getPrechapter:function(){
    //如果没有章节就放回
    if (this.data.chapter.length == 0) {
      wx.navigateBack({
        delta: 1
      })
    }
     var id=this.data.book.novelId
     var no=wx.getStorageSync("novel-"+id)
    //如果是第一次读取
    no= no==""?0:no-1
    this.loadGetChapter(this.data.chapter[no])


  },

  /**
   * 加载对应章节
   */
  loadGetChapter:function(chapter){
    var id = this.data.book.novelId
    var that = this
    app.sendRequest({
        url: 'chapter/context',
        method: 'get',
        data: {
          "nid": id,
          "rd": chapter.chapterNo
        },
        header: {
          'content-type': 'application/json;'
        },
        succfn: function (data) {
          if (data.code == 1000) {
            that.setData({
              titile: chapter.title,
              context:data.datas
            })
        

            //判断是否为最后一章
            if (that.data.chapter.length == chapter.chapterNo) {
              that.setData({
                "nextClick": "prohibited",
                "preClick":''
              })
            }
            //当前章节是在中间
             else if (chapter.chapterNo > 1 && chapter.chapterNo < that.data.chapter.length){
              that.setData({
                "nextClick": "",
                "preClick": ''
              })
            }
            //既不在中间也不再最后，那么就是在第一章
            else{
              that.setData({
                "nextClick": "",
                "preClick": 'prohibited'
              })
            } 

            //记录本次加载的章节
            wx.setStorageSync("novel-"+id,chapter.chapterNo)
            that.setData({
              "chapterNo": chapter.chapterNo
            })

          }
        },
        failfn: function (data) {
          console.log(data)
        }
      })
    
  },
  /**
   * 从目录页跳转过的，用于切换章节
   */
  gotochapter:function(e){
    
    this.loadGetChapter(e.detail.chapter)
    this.table(e)
  },

  /**
   * 加载章节列表
   */
  loadChapter:function(){
    var that=this
    //获取章节
    app.sendRequest({
      url: 'chapter/list',
      method: 'get',
      data: {
        "nid": this.data.book.novelId
      },
      header: {
        'content-type': 'application/json;'
      },
      succfn: function (data) {
        if (data.code == 1000) {
          that.setData({
            chapter: data.datas
          })

          //加载章节
          that.getPrechapter()
        }
      },
      failfn: function (data) {
        console.log(data)
      }

    })

  },

  /**
   * 更具样式名，切换样式
   */
  switchTheme: function (theme) {
    //判断当前是什么样式
    switch (theme) {
      case "default":
        this.defaultRead()
        break
      //夜间
      case "atnight":
        this.atnight()
        break
      //护眼
      case "green":
        this.green()
        break
      default:
        console.log('啥也不是哦')
    }
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  /**
   * 变大字体
   */
  bigFont:function(){
      this.setData({
        fontSize:this.data.fontSize+0.5
      })
  },

  /**
   * 变小字体
   */
  smailFont:function(){
    this.setData({
      fontSize: this.data.fontSize - 0.5
    })
  },

  /**
   * 设置字体
   */
  setFont:function(e){

    if (e.detail.type=="+"){
      this.bigFont()
    }else{
      //减少
      this.smailFont()
    }
    wx.setStorageSync("font-size", this.data.fontSize)
   
  },

  /**
   * 下一章事件触发
   */
  next:function(){
    //判断是否能够加载下一掌
    if (this.data.chapter.length <= this.data.chapterNo){
         return;
    } 
    //这里获取的是chapterNo是从1开始，而chapter存储的是从0开始，所以不需要+1
    this.loadGetChapter(this.data.chapter[this.data.chapterNo])
    wx.pageScrollTo({
      scrollTop: 0,
      duration: 10
    })
 },

  /**
     * 上一章事件触发
     */
  pre: function () {
    //判断是否能够加载上一掌
    if (this.data.chapterNo <= 1) {
      return;
    }
    //这里获取的是chapterNo是从1开始，而chapter存储的是从0开始，所以需要-2
    this.loadGetChapter(this.data.chapter[this.data.chapterNo-2])
    wx.pageScrollTo({
      scrollTop: 0,
      duration: 10
    })
  },


  /**
   * 呼唤目录
   */
  table:function(e){
    
      this.setData({
        table:!this.data.table,
        menu:false
      })
  },
  /**
   * 切换主题
   */
  theme:function(e){
      this.switchTheme(e.detail.name)
  },
  /**
   * 
   * 呼出或隐藏菜单
   */
  out:function(e){
      this.setData({
        menu: !this.data.menu
      })
  }
})