

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}




/**
 *  带回调函数的的Toast提示
 * @param {string} title
 * @param {string} icon
 * @param {int} duration
 * @param {callback} success
 */
function showToast(title = "成功啦", icon = "success", duration = 2000, success) {
  wx.showToast({
    title: title,
    icon: icon,
    duration: (duration <= 0) ? 2000 : duration,
    success: function () {
      if (success) {
        success();
      }
    }
  });
}

/**
 * 成功提示
 * @param {string} title
 * @param {int} duration
 */
function showSuccess(title = "成功啦", duration = 2000) {
  wx.showToast({
    title: title,
    icon: 'success',
    duration: (duration <= 0) ? 2000 : duration
  });
}

/**
 * loading提示
 * @param {string} title
 * @param {int} duration
 */
function showLoading(title = "加载中", mask = true, success, fail, complete) {
  wx.showLoading({
    title: title,
    mask: mask,
    success: success,
    fail: fail,
    complete: complete
  });
}

/**
 * 隐藏loading
 */
function hideLoading() {
  wx.hideLoading();
}

/**
 * 隐藏提示框
 */
function hideToast() {
  wx.hideToast();
}

/**
 * 显示带取消按钮的消息提示框
 * @param {string} title
 * @param {string} content
 * @param {callback} confirm
 * @param {boolean} showCancel
 */
function alertViewWithCancel(title = "提示", content = "消息提示", confirm, showCancel = true) {
  wx.showModal({
    title: title,
    content: content,
    showCancel: showCancel,
    success: function (res) {
      if (res.confirm) {
        (typeof confirm === 'function') && confirm();
      }
    }
  });
}

/**
 * 显示不带取消按钮的消息提示框
 * @param {string} title
 * @param {string} content
 * @param {callback} confirm
 */
function alertView(content = "消息提示", confirm) {
  alertViewWithCancel('', content, confirm, false);
}

/**
 * 从本地缓存中异步获取指定 key 对应的内容
 * @param {string} keyname
 * @param {callback} success
 * @param {callback} fail
 */
function getstoragedata(keyname, success, fail) {
  wx.getStorage({
    key: keyname,
    success: function (res) {
      if (res.data) {
        success(res);
      }
    },
    fail: function () {
      fail();
    }
  });
}
function formatTime(year) { //获得当前时间的年月日
  var date = new Date()
  var year = year ? (parseInt(date.getFullYear()) + year) : date.getFullYear()
  var month = date.getMonth() + 1
  var day = date.getDate()

  var hour = date.getHours()
  var minute = date.getMinutes()
  var second = date.getSeconds()


  return [year, month, day].map(formatNumber).join('-') //+ ' ' + [hour, minute, second].map(formatNumber).join(':')
}

function formatYestoday() { //获得昨天的年月日
  var day1 = new Date();
  day1.setTime(day1.getTime() - 24 * 60 * 60 * 1000);
  var s1 = day1.getFullYear() + "-" + (day1.getMonth() + 1) + "-" + day1.getDate();
  return s1
}




/**
 * 静态数据变量
 */
var constData = (function () {

  return {
    /**默认当前页 */
    pageNo: 1,
    /**默认每页条数 */
    pageSize: 10
  }
})();

/**
 * 验证器
 */
var validator = (function () {

  return {
    /**
     * 判断是否是手机号码
     */
    isMobile: function (str) {
      return /^(13|14|15|16|17|18|19)\d{9}$/i.test(str);
    },

    /**
     * 判断是否是电话号码
     */
    isTel: function (str) {
      return /^((\(\d{2,3}\))|(\d{3}\-))?(\(0\d{2,3}\)|0\d{2,3}-)?[1-9]\d{6,7}(\-\d{1,4})?$/i.test(str);
    },

    /**
     * 判断是否是邮箱地址
     */
    isEmail: function (str) {
      return /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i.test(str);
    },

    /**
     * 判断是否是邮编
     */
    isZip: function (str) {
      return /^[\d]{6}$/.test(str);
    },

    /**
     * 判断是否是中文
     */
    isChinese: function (str) {
      return /^[\u4e00-\u9fa5]+$/i.test(str);
    },

    /**
     * 判断是否是英文
     */
    isEnglish: function (str) {
      return /^[A-Za-z]+$/i.test(str);
    },

    /**
     * 判断是否是发动机号
     */
    isCarEngineNo: function (str) {
      return /^[a-zA-Z0-9]{16}$/.test(str);
    }

  };
})();


/**
 * 时间错转字符串
 */
function timestampToTime(timestamp) {
  var date = new Date(timestamp * 1000);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
  var Y = date.getFullYear() + '-';
  var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
  var D = date.getDate() + ' ';
  var h = date.getHours() + ':';
  var m = date.getMinutes();
  return Y + M + D + h + m;
}







/**
 *  模块映射
 */
module.exports = {
  showToast: showToast,
  showSuccess: showSuccess,
  showLoading: showLoading,
  hideLoading: hideLoading,
  hideToast: hideToast,
  alertViewWithCancel: alertViewWithCancel,
  alertView: alertView,
  getstoragedata: getstoragedata,
  formatTime: formatTime,
  constData: constData,
  validator: validator,
  formatYestoday: formatYestoday,
  timestampToTime: timestampToTime
};
