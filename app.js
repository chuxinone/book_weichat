var setting = require('utils/setting.js');
var util = require('utils/util.js');
//app.js
App({
  onLaunch: function () {
    // 展示本地存储能力
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)

    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
      }
    })
    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              // 可以将 res 发送给后台解码出 unionId
              this.globalData.userInfo = res.userInfo

              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (this.userInfoReadyCallback) {
                this.userInfoReadyCallback(res)
              }
            }
          })
        }
      }
    })
  },
  setting: setting,
  util:util,
  sendRequest: function (param) {
    var that = this,
      data = param.data || {},
      header = param.header
    // if (param.method && param.method.toLowerCase() == 'get') {
    //   header = header || {
    //     'content-type': 'application/x-www-form-urlencoded;'
    //   }
    // }

    if (!param.hideLoading) {  //请求状态
      wx.showLoading({
        title: '请求中...',
      });
    }

    if (this.globalData.openId) {
      data.openId = this.globalData.openId
    }


    wx.request({
      url: that.setting.api + param.url,
      data: data,
      method: (param.method && param.method.toUpperCase()) || 'POST',
      header: header || {
        'content-type': 'application/x-www-form-urlencoded'
      },
      success: function (res) {
        console.log(res.data)
        typeof param.succfn == 'function' && param.succfn(res.data)
      },
      fail: function (res) {

        wx.showModal({
          content: '请求失败 ' + res.errMsg
        })
        typeof param.failfn == 'function' && param.failfn(res.data);
      },
      complete: function (res) {
        wx.hideLoading();
        wx.hideToast();
        typeof param.completefn == 'function' && param.completefn(res.data);
      }
    })
  },
  globalData: {
    userInfo: null
  }
})