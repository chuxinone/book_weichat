// components/table/table .js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    css: Object,
    chapter:Object,
    book:Object
  },

  /**
   * 组件的初始数据
   */
  data: {
    readerCss:{},
    chapter:[],
    windowWidth:0,
    windowHeight:0,
    table:true,
    book:null,
    index: 0,
    array: [],
    datas:[],
    toView:""
  },


  // 初始化完成
  attached() { 

    //设置css
    var that=this
    var readerCss=this.data.readerCss;
    readerCss = this.properties.css

    this.setData({
      readerCss: readerCss,
      chapter:this.properties.chapter,
      book:this.properties.book
    })

    //获取设备信息
    wx.getSystemInfo({
      success(res) {
        that.setData({
          windowWidth: res.windowWidth,
          windowHeight: res.windowHeight
        })
      }
    })

    this.segmentation()

    //跳转到对应的章节子项
    var no = wx.getStorageSync("novel-" + this.data.book.novelId)

    //更具章节获取页数
    var page = this.chapterNoPage(parseInt(no))
    this.chapterlist(page)
    this.setData({
      index: page
    })

    this.redChapter()

  
  },


  /**
   * 组件的方法列表
   */
  methods: {
    onTap: function () {
      var myEventDetail = {} // detail对象，提供给事件监听函数
      var myEventOption = {} // 触发事件的选项
      
      this.triggerEvent('myevent', myEventDetail, myEventOption)
    },
    hidden:function(){
      this.setData({
        table:!this.data.table
      })
    },
    /**
     * 分卷
     * 1-100为一个栏
     */
    segmentation:function(){
      //150
      var chapter_length = this.data.chapter.length / 100

      var column = parseInt(chapter_length) 
      // array
      var range = new Array()
      for (var i = 0; i <= column; i++) {
        var range_str = i * 100 + 1 + ''
        var k = (i+1)*100
        if (k > this.data.chapter.length){
          range_str += '-' + this.data.chapter.length
        }else{
          range_str+='-'+k
        }
        range[i]=range_str
      }

      this.setData({
        array: range
      })

   

    },

    /**
     * 根据章节数来判断该章节在第几页
     */
    chapterNoPage:function(no){
      //获取总页数
      var array =this.data.array
      var page =parseInt((no-1)/100)
      if(page>(array.length-1)){
        return 0
      }
      return page
    },
    /**
     * 根据页数，返回对应的章节列表
     */
    chapterlist:function(page){
      var chapter=new Array()
      var k = (parseInt(page) + 1) * 100
      console.log(k)
      for (var j = page * 100 + 1,i=0; j <= k && j <= this.data.chapter.length; j++,i++) {
        chapter[i] = this.data.chapter[j - 1]
      }
      this.setData({
        datas: chapter
      })
    },

    /**
     * 根据对应的章节NO，来跳转对应的章节
     */
    gotoChapter:function(no){
      var myEventDetail = {
        "chapter": no.currentTarget.dataset.chapter
      } // detail对象，提供给事件监听函数
      var myEventOption = {} // 触发事件的选项

      this.triggerEvent('gotochapter', myEventDetail, myEventOption)

      console.log(no.currentTarget.dataset.chapter)
    },
    
    /**
     * 显示选定的章节为红色
     */
    redChapter:function(){
      this.setData({
        toView: "chapter-"
      })
      //跳转到对应的章节子项
      var no = wx.getStorageSync("novel-" + this.data.book.novelId)
   
      //更具章节获取页数
      var page = this.chapterNoPage(parseInt(no))
      console.log(page)
      console.log(this.data.index)
      if(page!=this.data.index){
        return
      }
      console.log(no)
      var index = no - 1 - page * 100
      console.log(index)
      this.setData({
        toView: "chapter-" + index
      })
    },
    bindPickerChange: function (e) {
      console.log('picker发送选择改变，携带值为', e.detail.value)
      var i = e.detail.value
      this.chapterlist(i)
      this.setData({
        index: e.detail.value,
       })

      this.redChapter()
    }
  }
 
})
