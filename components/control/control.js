// components/control/control.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    book: Object,
    chapter: Object
  },

  /**
   * 组件的初始数据
   */
  data: {
    windowWidth:0,
    windowHeight: 0,
    progress:-10,
    bigdisable:"",
    smaildisable:"",
    theme:"",
    atnightTheme:"",
    greenTheme:"",
    next_disable:"",
    pre_disable: "",
    no:0
  },

  // 初始化完成
  attached() {
    var that = this;

    //设置分辨率
    wx.getSystemInfo({
      success(res) {
        that.setData({
          windowWidth: res.windowWidth,
          windowHeight: res.windowHeight
        })
      }
    })

    //获取字体进度条
    var progress= wx.getStorageSync("font-progress")
    // console.log(progress)

    //获取当前在多少章
    var no = wx.getStorageSync("novel-" + this.properties.book.novelId)

    if (progress == 0){
      progress=50
      wx.setStorageSync("font-progress", progress)
    }
    this.setData({
      progress: progress,
      no:no
    })

   


    //设置当前的主题显示
    var theme = wx.getStorageSync("readTheme")
    this.switchTheme(theme)

    this.disableFont()

    //检查是否有下一章节或上一章节
    this.disable_chapter()
   
  },

  /**
   * 组件的方法列表
   */
  methods: {
    /**
     * 更具不同的主题切换不同的操作
     */
    switchTheme: function (theme) {
      //判断当前是什么样式
      switch (theme) {
        case "default":
          this.setData({
            "theme":"theme",
            "atnightTheme": "",
            "greenTheme": ""
          })
          break
        //夜间
        case "atnight":
          this.setData({
            "theme": "",
            "atnightTheme": "theme",
            "greenTheme": ""
          })
          break
        //护眼
        case "green":
          this.setData({
            "theme": "",
            "atnightTheme": "",
            "greenTheme": "theme"
          })
          break
        default:
          console.log('啥也不是哦')
      }

    },
    /**
     * 是否禁用字体大小
     */
    disableFont: function(){

        //判断 大 按钮
      if (this.data.progress >= 100) {
        this.setData({
          "bigdisable": "disable"
        })
      }

      else if (this.data.progress <= 0) {
        this.setData({
          "smaildisable": "disable"
        })
      }
      
      else if (this.data.bigdisable == "disable" || this.data.smaildisable == "disable" ){
        this.setData({
          "smaildisable": ""
        })

        this.setData({
          "bigdisable": ""
        })

      }

      // console.log(this.data.progress)


    },
    /**
     * 设置字体大小
     */
    setFont:function(e){
      var myEventDetail = { 'type': e.target.dataset.type } // detail对象，提供给事件监听函数
      var myEventOption = {} // 触发事件的选项


      if (e.target.dataset.type == "+") {
        this.setData({
          progress: this.data.progress + 10
        })
      } else {
        this.setData({
          progress: this.data.progress - 10
        })
      }



      this.disableFont()


      if (this.data.progress <= 100 && this.data.progress >=0) {
        this.triggerEvent('font', myEventDetail, myEventOption)
      }

      if (this.data.progress > 100) {
        this.setData({
          "progress": 100
        })

      } else if (this.data.progress < 0) {
        this.setData({
          "progress": 0.1
        })

      }


      wx.setStorageSync("font-progress", this.data.progress)
    
      
    },
    /**
     * 打开目录
     */
    opentable:function(e){
      this.triggerEvent('table', {}, {})
    },
    /**
     * 切换主题
     */
    theme:function(e){
      this.switchTheme(e.target.dataset.theme)
      this.triggerEvent('theme', { "name": e.target.dataset.theme}, {})
    },
    /**
     *   判断当前是否含有下一章或上一章
     */
    disable_chapter:function(){
      var no =this.data.no
      console.log(no)
      if (this.properties.chapter.length == no) {
        this.setData({
          next_disable: "disable",
          pre_disable: ""
        })
      } else if (no == 1) {
        this.setData({
          next_disable: "",
          pre_disable: "disable"
        })
      } else {
        this.setData({
          next_disable: "",
          pre_disable: ""
        })
      }

    },
    /**
     * 切换上一章
     */
    on_pre:function(e){
      if (this.data.pre_disable != "") return
      this.triggerEvent('pre', {}, {})
      this.setData({
        no: parseInt(this.data.no) - 1
      })
      this.disable_chapter()

    },
    /**
       * 切换下一章
       */
    on_next: function (e) {
      if (this.data.next_disable != "") return
      this.triggerEvent('next', {}, {})
      this.setData({
        no:parseInt(this.data.no)+1
      })
      this.disable_chapter()
        
    }
  }
})
