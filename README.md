
#### 项目介绍
不想下APP，不想看充钱，你还想看书？ 没问题。快快关注小程序，畅快看书吧

#### 简单说明

开发周期：3个月



#### 进度
目前正常开发到目录阶段
阅读小说界面已完成上一章和下一章的功能。现在需要完成控件板上的 上一章和下一章功能(解决)


##### 问题
目录：

  在目录中，的总章节总是为100。自动跳转只能显示当前页的章节，超过当前页，无法跳转(解决)


##### 2018/7/11

- ** 完成一个页面  **

![输入图片说明](https://images.gitee.com/uploads/images/2018/0711/133225_e127b5fc_1565845.png "TIM截图20180711133206.png")



#### 界面展示

- ** 首页  **
![输入图片说明](https://images.gitee.com/uploads/images/2019/0102/143646_bf39d502_1565845.png "TIM截图20190102143505.png")

- ** 小说详情  **
![输入图片说明](https://images.gitee.com/uploads/images/2019/0102/143725_ce0233ba_1565845.png "TIM截图20190102143505.png")

- ** 搜索界面  **
![输入图片说明](https://images.gitee.com/uploads/images/2019/0102/143816_91ee4655_1565845.png "TIM截图20190102143505.png")

- ** 搜索列表  **
![输入图片说明](https://images.gitee.com/uploads/images/2019/0102/143912_6c0231b9_1565845.png "TIM截图20190102143505.png")

- ** 小说内容  **
![输入图片说明](https://images.gitee.com/uploads/images/2019/0102/144032_bb027c81_1565845.png "在这里输入图片标题")

- ** 主题切换  **
![输入图片说明](https://images.gitee.com/uploads/images/2019/0102/144134_031887ef_1565845.png "TIM截图20190102143505.png")

- ** 目录 **

![输入图片说明](https://images.gitee.com/uploads/images/2019/0102/144207_3607ac0c_1565845.png "TIM截图20190102143505.png")